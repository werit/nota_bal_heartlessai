local sensorInfo = {
	name = "AddAllUnassignedUnits",
	desc = "Get all units that are not assigned to blackboard variable",
	author = "Werit",
	date = "2018-06-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 5

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function()

  if bb.myUnits == nil then
    bb.myUnits={}
  end
  local unitsToAddToBb = {}
  local newUnits = Sensors.nota_bal_heartlessai.GetAllMyUnits()

  for i=1,#newUnits do
  --  local unitAlreadyExists = false
    --[[for j=1,#bb.myUnits do
      if bb.myUnits[j]== newUnits[i] then
        unitAlreadyExists= true
      end
    end]]--
    if bb.myUnits[newUnits[i]] == nil then
        bb.myUnits[newUnits[i]] = true
    end
    --[[if not unitAlreadyExists then
      unitsToAddToBb[#unitsToAddToBb + 1] = newUnits[i]
    end]]
  end

  --[[for i=1,#unitsToAddToBb do
    bb.myUnits[#bb.myUnits + 1] = unitsToAddToBb[i]
  end]]

  --[[return unitsToAddToBb]]

end
