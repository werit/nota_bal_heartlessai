local sensorInfo = {
	name = "UnitsFromBb",
	desc = "Get Unit Ids From Bb",
	author = "Werit",
	date = "2018-06-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function()
  if bb.myUnits == nil then
    return {}
  end
  local unts ={}
  for unitId,value in pairs(bb.myUnits) do
    unts[#unts+1] = unitId
  end
  return unts
end
