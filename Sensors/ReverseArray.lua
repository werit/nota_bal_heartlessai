local sensorInfo = {
	name = "ReverseArray",
	desc = "Generate reverse array from an array",
	author = "Werit",
	date = "2019-05-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
return function(array)
	reverseArray = {}
  for i=1,#array do
    reverseArray[#array-i+1] = array[i]
  end
	return reverseArray
end
