local sensorInfo = {
	name = "ClearBbFromDead",
	desc = "Remove units that are dead or invalid",
	author = "Werit",
	date = "2018-06-20",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function()
  if bb.myUnits == nil then
    return
  end
  for unitId,value in pairs(bb.myUnits) do
    if not Spring.ValidUnitID(unitId) then
      bb.myUnits[unitId] = nil
    end

  end
end
