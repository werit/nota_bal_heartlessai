local sensorInfo = {
	name = "GetUnitWeaponInfo",
	desc = "Returns UnitID,WeaponId,WeaponRange",
	author = "Werit",
	date = "2018-05-07",
	license = "NA",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
-- it is expected that we would not need completely up-to-date information
-- this is copy from example
    period = EVAL_PERIOD_DEFAULT,
    }
end

return function()
  if #units<1 then
    return
  end


  local enemyUnits = Sensors.core.EnemyUnits()
  local enemyUNitId = enemyUnits[1]
  local enemyDefId = Spring.GetUnitDefId(enemyUNitId)
  local weaponDefId = UnitDefs[enemyDefId].weapons[1].weaponDef
  --WeaponDefs je globalna tabulka, ktora je pristupna v kazdom skripte
  local weaponRange = WeaponDefs[weaponDefId].range
  local weaponRange = WeaponDefs[weaponDefId].name
  return {
    enemyUNitId,
    weaponDefId,
    weaponRange
  }

end
