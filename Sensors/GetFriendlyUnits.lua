local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(center, radius)
	local team =  Spring.GetLocalTeamID()
	local my_units = Spring.GetUnitsInCylinder(center.x, center.z, radius, team)
	

	return my_units
end