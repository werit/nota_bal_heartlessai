local sensorInfo = {
	name = "GroupNotMoving",
	desc = "Return true or false whether group of unit is moving or not.",
	author = "Werit",
	date = "2019-04-18",
	license = "notAlicense",
}
local EVAL_PERIOD_DEFAULT = 1 -- 1sec caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
local VELO_THRESHOLD = 1
-- @description return true if all units are in motion (not stuck etc) and false otherwise
return function()
    for i=1,#units do
      local velx, vely, velz, velLength = Spring.GetUnitVelocity(units[i])
      if math.abs(velx)+math.abs(vely)+math.abs(velz) <VELO_THRESHOLD then
        return true
    end
  end
  return false
end
