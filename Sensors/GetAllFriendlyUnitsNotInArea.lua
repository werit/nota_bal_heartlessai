local sensorInfo = {
	name = "GetAllFriendlyUnitsNotInArea",
	desc = "Return all units in team in in certain area",
	author = "Werit",
	date = "2019-05-14",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 10 -- 10 sec caching
local CENTRE =Vec3(0,0,0)

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local SpringGetUnitPosition = Spring.GetUnitPosition

local function compareByDistance(firstUnitId,secondUnitId)
    local posX,posY,posZ = SpringGetUnitPosition(firstUnitId)
    local posXSec,posYSec,posZSec = SpringGetUnitPosition(secondUnitId)
    return Vec3(posX,posY,posZ):Distance(CENTRE) < Vec3(posXSec,posYSec,posZSec):Distance(CENTRE)
end

--Have to be tested
return function(centre,radius)
  local teamID =  Spring.GetLocalTeamID()
  local thisTeamUnits = Spring.GetTeamUnits(teamID)
	CENTRE = centre
	local unitsToSave ={}

		for u=1, #thisTeamUnits do
      --return{centre,radius,thisTeamUnits[u]}
      if not Sensors.nota_bal_heartlessai.IsUnitInArea(centre,radius,thisTeamUnits[u])
        and not UnitDefs[Spring.GetUnitDefID(thisTeamUnits[u])].cantBeTransported
      then
        --local posX,posY,posZ =Spring.GetUnitPosition(thisTeamUnits[u])
        unitsToSave[#unitsToSave + 1] =thisTeamUnits[u]
      end
        end
        table.sort(unitsToSave,compareByDistance)
        unitsToSaveSorted = {}
        for i,unit in ipairs(unitsToSave) do table.insert(unitsToSaveSorted,unit) end
    return unitsToSaveSorted
end
