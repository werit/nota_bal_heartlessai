local sensorInfo = {
	name = "GetAllMyUnits",
	desc = "ReturnAllMyUnitsIds",
	author = "Werit",
	date = "2018-0ž-ľé",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

return function()
  local myTeamId = Spring.GetMyTeamID()
  return Spring.GetTeamUnits(myTeamId)
end
