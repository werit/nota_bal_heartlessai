local sensorInfo = {
	name = "Position",
	desc = "Return data of unit position.",
	author = "Werit",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description return current wind statistics
return function(unitId)
	if unitId == nil then
		return FAIL
	end
  --it is impossible to read data directly from spring in tree...so I have to implement sensor....seriously? there is no better way?
	local bpx, bpy, bpz = Spring.GetUnitPosition(unitId)
	return {
		bpx = bpx,
		bpy = bpy,
		bpz = bpz
	}
end
