local sensorInfo = {
	name = "AreaInHeightFinder",
	desc = "Returns areas below or above certain threshold",
	author = "Werit",
	date = "2019-04-23",
	license = "NA",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 10 -- caching 10 sec
function getInfo()
    return {
-- it is expected that we would not need completely up-to-date information
-- this is copy from example
    period = EVAL_PERIOD_DEFAULT,
    }
end

local function CheckNeigbors( row , col)

end

local function ConditionCheck(valueToCheck,threshold,belowOrAbove)
	if belowOrAbove == "below" then
		if valueToCheck.height <= threshold then
			return true
		end
	else
		if belowOrAbove == "above" and valueToCheck.height >= threshold then
			return true
		end
	end
	return false
end

local function findAreas(grid,threshold,takeAreasBelowOrAbove)
	foundAreas = {}
	local validAreaStack = LocalStack:new{}
	local borderAreaStack = LocalStack:new{}
	local openedAreaStack = LocalStack:new{}

	grid[0][0]:state = Opened
	openedAreaStack:put(grid[0][0])

		local changeHappend = true
	while changeHappend	do
		changeHappend = false
		while validAreaStack:isEmpty and not openedAreaStack:isEmpty do
			local topNode = openedAreaStack:pop()
			if ConditionCheck(topNode,threshold,takeAreasBelowOrAbove) then
				validAreaStack.put(topNode)
			else
				borderAreaStack.put(topNode)
			end
		end
		if not validAreaStack:isEmpty  then
			changeHappend = true
			foundAreas[#foundAreas+1] = ResolveValidArea(grid,validAreaStack,borderAreaStack,threshold,takeAreasBelowOrAbove)
		end
		if not changeHappend and not borderAreaStack:isEmpty then
			changeHappend = true
			AddAllNotProcessedNeighborsToStack(grid,borderAreaStack:pop(),openedAreaStack)
		end
end
	return foundAreas
end

-----------------------
--Resolve valid area --
-----------------------
local function ResolveValidArea(grid,validAreaStack,borderAreaStack,threshold,takeAreasBelowOrAbove)
	local openedAreaStack = LocalStack:new{}
	--openedAreaStack:put(validAreaStack:pop())
	area = {}
	while not validAreaStack:isEmpty do
		AddAllNotProcessedNeighborsToStack(grid,validAreaStack:pop(),openedAreaStack)
		while not openedAreaStack:isEmpty do
			local topNode = openedAreaStack:pop()
			if ConditionCheck(topNode,threshold,takeAreasBelowOrAbove) then
				validAreaStack.put(topNode)
				area[#area+1] = topNode
			else
				borderAreaStack.put(topNode)
			end
		end

	end
	return area
end
function AddAllNotProcessedNeighborsToStack(grid,node,stackToAddNeighbours)
-- TODO
end
------------------
-- STACK ---------
------------------
LocalStack = {count = 0,
				nodes={}}
function LocalStack:new(o)
	o = o or {}   -- create object if user does not provide one
	setmetatable(o, self)
	self.__index = self
	return o
end
-- Add element to the top of stack
function LocalStack:put(node)
	self.count = self.count + 1
	self.nodes[self.count]= node
end
-- Removes top-most element from stack and returns it
function LocalStack:pop()
	retNode = self.nodes[self.count]
	self.nodes[self.count] = nil
	self.count = self.count - 1
	return retNode
end
-- Check whether stack is empty
function LocalStack:isEmpty()
	return self.count == 0
end
------------------
-- NODE ----------
------------------

-- Expected states:
local NotOpened = 'notOpen'
local Opened = 'open'
local Closed = 'closed'
Node = {posX = 0,
				posZ = 0,
				height = 0,
				state = NotOpened}
function Node:new (o)
		o = o or {}   -- create object if user does not provide one
		setmetatable(o, self)
		self.__index = self
		return o
	end

local GameSizeX = Game.mapSizeX
local GameSizeZ = Game.mapSizeZ
-- generujem grid z mapy
-- rozsekam si mapu na mensie
function makeGridFromMap()
  -- grid mapy
  grid  ={}
	  for i=0,GameSizeX-1 do
    grid[i] = {}
    for j=0,GameSizeZ-1 do
      grid[i][j] = Node:new{posX = i,posZ = j,height=Spring.GetGroundHeight(i,j)}
    end
  end
  return grid
end

-- toto je funkcia, ktora sa fakt vracia, ale netusim, kto a ako ju cita
return function(areaHeightRequiered,takeAreasBelowOrAbove)
  -- prepare grid
  areas = makeGridFromMap()
  -- search grid for hills and return result
  return findAreas(areas,areaHeightRequiered,takeAreasBelowOrAbove)
end
