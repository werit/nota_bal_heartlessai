local sensorInfo = {
	name = "PointsOnEdges",
	desc = "Return points on 3 edges of map",
	author = "Werit",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 1000 -- caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(pointCount)

if pointCount<3 then
  error("Number of points is too low, you need at least 3. They would represent start of each edge.")
	return {}
end
local leftCnt = math.floor(pointCount/3+0.5)
local rightCnt = math.floor(pointCount/3+0.5)
local downCnt = pointCount - leftCnt - rightCnt
local GameSizeX = Game.mapSizeX
local GameSizeZ = Game.mapSizeZ

local locations = {}
-- left edge
local distOnLeftEdge = GameSizeZ/leftCnt
  for i=1,leftCnt do
      locations[#locations+1] = Vec3(0,Spring.GetGroundHeight(0,i*distOnLeftEdge),i*distOnLeftEdge)
  end
  -- middle edge
  local distOnMiddleEdge = GameSizeX/downCnt
  for i=1,downCnt do
      locations[#locations+1] = Vec3(i*distOnMiddleEdge,Spring.GetGroundHeight(i*distOnMiddleEdge,GameSizeZ),GameSizeZ)
  end
-- right edge
local distOnRightEdge = GameSizeZ/rightCnt
  for i=1,rightCnt do
      locations[#locations+1] = Vec3(GameSizeX,Spring.GetGroundHeight(GameSizeX,i*distOnRightEdge),i*distOnRightEdge)
  end

  return locations
end
