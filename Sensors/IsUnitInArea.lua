local sensorInfo = {
	name = "IsUnitInArea",
	desc = "Return true if unit is in cylindrical area specified by parameter",
	author = "Werit",
	date = "2019-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
--Have to be tested
return function(centre,radius,unitID)
  local unitPosition = Sensors.nota_bal_heartlessai.Position(unitID)
  if unitPosition~=nil then
--return {unitPosition.bpx,centre.x}
    if math.sqrt(math.pow(unitPosition.bpx -centre.x,2) + math.pow(unitPosition.bpz -centre.z,2))< radius then
      return true
    end
  else
    error("invalid position. Possible wrong unitID.")
  end
  return false
end
