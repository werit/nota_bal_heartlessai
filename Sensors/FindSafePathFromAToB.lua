--{Vec3(600,Spring.GetGroundHeight(600,200),600),Vec3(300,Spring.GetGroundHeight(300,300),300),Vec3(500,Spring.GetGroundHeight(500,500),500)}

local sensorInfo = {
	name = "FindSafePathFromAToB",
	desc = "Find safe path from A to B for an unit",
	author = "Werit",
	date = "2019-05-31",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 --extended chaching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unitId, startPosition, destination)
 return {
         startPosition,
         destination
        }
end
