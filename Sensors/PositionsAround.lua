local sensorInfo = {
	name = "PositionsAround",
	desc = "Generate array of positions.",
	author = "Werit",
	date = "2019-04-29",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end
return function(position,distance,neededPos)
	positionsAround = {}
	local numShifts = 8
	local dist = distance
	local circle = 360
	local shift = circle/numShifts

	while #positionsAround < neededPos do
		for i=1,numShifts do
			local newX = position.x + math.sin(i* shift)*dist
			local newZ = position.z + math.cos(i*shift)*dist
			positionsAround[#positionsAround + 1] = Vec3(newX,Spring.GetGroundHeight(newX,newZ),newZ)
		end
		numShifts = numShifts + 5
		dist = dist + 80
		shift = circle/numShifts
	end
	return positionsAround
end
