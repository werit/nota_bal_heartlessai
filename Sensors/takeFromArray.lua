local sensorInfo = {
	name = "TakeFromArray",
	desc = "Take 'n' elements from 'array' starting on 'start' position including",
	author = "Werit",
	date = "2019-06-01",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(array,start,n)
  if array == nil or #array ==0 or
  start > #array then
    return {}
  end
  local count  = math.min(#array - start+1,n)
  local retArray = {}
  for i=start, start+count-1 do
    retArray[i - start + 1] = array[i]
  end
  return retArray
end
