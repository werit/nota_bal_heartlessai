local sensorInfo = {
	name = "GetEnemyUnitsInCylinder",
	desc = "Returns all enemy ids in specified cylinder",
	author = "Werit",
	date = "2019-06-21",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end
return function(center,radius)
  local enemyTeamIds = Sensors.core.EnemyTeamIDs()
  local enemiesInCylinder = {}

  for i=1,#enemyTeamIds do
    local enemies = Spring.GetUnitsInCylinder(center.x, center.z, radius, enemyTeamIds[i])
    for j=1,#enemies do
      enemiesInCylinder[#enemiesInCylinder + 1] = enemies[j]
    end
  end
  return enemiesInCylinder
end
