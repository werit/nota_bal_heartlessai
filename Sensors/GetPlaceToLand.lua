local sensorInfo = {
	name = "GetPlaceToLand",
	desc = "Sensor gets place and when the place is occupierd it will find new free space close by.",
	author = "Werit",
	date = "2019-06-11",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1
local AREA_AROUND_RECT = 50
local EXTENDED_THRESHOLD = 100

function getInfo()
    return {
        period = EVAL_PERIOD_DEFAULT
    }
end

local function UnitsInDesiredRect(dropPt,unitdefId)
	local halfSizeX = 50--math.floor(UnitDefs[unitdefId].footprintX/2 +0.5)
	local halfSizeZ = 50--math.floor(UnitDefs[unitdefId].footprintZ/2 +0.5)
  return Spring.GetUnitsInRectangle(dropPt.x - halfSizeX,
	 													dropPt.z - halfSizeZ,
														dropPt.x + halfSizeX,
														dropPt.z + halfSizeZ)
end

return function(centre,unitDefId)
  local dropPoint = centre
  local untsInArea = UnitsInDesiredRect(dropPoint,unitDefId)
  if untsInArea==nil or #untsInArea==0 then
    return dropPoint
	end


  local possiblePos = Sensors.nota_bal_heartlessai.PositionsAround(dropPoint,EXTENDED_THRESHOLD)
	Spring.Echo(dump(possiblePos))
	local cnt = 0
  while true do

		if cnt < 7 then
			cnt = cnt +1
		else
			--Spring.Echo("Still in infinite while")
			return centre
		end
    for i=1,#possiblePos do
      local pos = UnitsInDesiredRect(possiblePos[i],unitDefId)
      if pos==nil or #pos==0 then
        return possiblePos[i]
      end
    end
    EXTENDED_THRESHOLD = EXTENDED_THRESHOLD + 50
		possiblePos = Sensors.nota_bal_heartlessai.PositionsAround(dropPoint,EXTENDED_THRESHOLD)
  end
	return "UnexpectedFinish"
end
