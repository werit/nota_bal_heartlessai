function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move from position 'A' to position 'B' by path defined with first argument.",
		parameterDefs = {
			{
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<{50,50}>",
			},
			{
				name = "unitId", -- id of unit to move
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<IdOfTransportationUnit>",
			}
		}
	}
end


local THRESHOLD_DEFAULT = 80
local THRESHOLD_DELTA = 15
local THRESHOLD_MIDDLE_STEP = 50
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
  self.cntr = 0
  self.lastPosition = Vec3(0,0,0)
	self.threshold = THRESHOLD_DEFAULT
end

local function initOnce(self)
  if self.lastPosition == nil then
    self.lastPosition = Vec3(0,0,0)
  end
	if self.threshold == nil then
		self.threshold = THRESHOLD_DEFAULT
	end
  if self.cntr == nil then
    self.cntr = 0
  end
end

function Run(self, units, parameter)
	local transporter = parameter.unitId -- unit to move along the path
	if transporter == nil or not Spring.ValidUnitID(transporter) then
		--[[if  transporter ~= nil then
			Spring.Echo(dump(Spring.ValidUnitID(transporter))..dump(transporter).."IsDead")
		end]]
		return SUCCESS
	end

	local arr = {}
	arr = parameter.path -- array of points to take
  if arr == nill or #arr < 1 then
    return SUCCESS
  end

local defVect = Vec3(nil,nil,nil)
for i=1,#arr do
	if arr[i]==nil or arr[i] == defVect then
		return SUCCESS
	end
end
  	--Spring.Echo(dump(parameter.listOfUnits))
  initOnce(self)

	--repair positions if any is negative
	for i=1,#arr do
		arr[i].x = math.max(math.min(arr[i].x,Game.mapSizeX), 0)
		arr[i].z = math.max(math.min(arr[i].z,Game.mapSizeZ), 0)
		arr[i].y = math.max(Spring.GetGroundHeight(arr[i].x,arr[i].z), 0)
	end

 local posX,posY,posZ = SpringGetUnitPosition(transporter)
 local unitPos = Vec3(posX,posY,posZ)
  -- prepare command/move counter. It counts which moves were already done


  if self.cntr == 0 then
    self.cntr = self.cntr + 1
    SpringGiveOrderToUnit(transporter,CMD.MOVE,arr[self.cntr]:AsSpringVector(),{})
  end

  -- increase threshold if unit is not moving
  if (unitPos:Distance(self.lastPosition)<10) then
    self.threshold = self.threshold + THRESHOLD_DELTA
  else
    self.threshold = THRESHOLD_DEFAULT
  end

  self.lastPosition = unitPos

  -- correct threshold if not last step
  local thresholdAdj = self.threshold
  if self.cntr < #arr then
    thresholdAdj = thresholdAdj + THRESHOLD_MIDDLE_STEP
  end

  -- if almost there
  if unitPos:Distance(arr[self.cntr]) < thresholdAdj then
    -- assign new move command
    self.cntr = self.cntr + 1
    -- check whether there are any commands to give
    if self.cntr <= #arr then
      SpringGiveOrderToUnit(transporter,CMD.MOVE,arr[self.cntr]:AsSpringVector(),{})
      return RUNNING
    else
      -- no more moves to give
      return SUCCESS
    end
  else
    return RUNNING
  end
return SUCCESS

end



function Reset(self)
	ClearState(self)
end
