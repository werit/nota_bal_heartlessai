function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Laod 5 defined unitId",
		parameterDefs = {
			{
				name = "numberToStartFromInList",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<1>",
			},
			{
				name = "listOfUnits", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<list of unitIds>",
			}
		}
	}
end

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
end

function Run(self, units, parameter)
	local listOfUnits = parameter.listOfUnits -- arr of UNITS
	local startIndex = parameter.numberToStartFromInList -- number
	--Spring.Echo(dump(parameter.listOfUnits))

	local cmdID = CMD.LOAD_UNITS
  if startIndex > #listOfUnits then
    return SUCCESS
  end
	for i=startIndex,math.min(#listOfUnits,startIndex+5) do
    --Spring.Echo(dump(listOfUnits[i]))
    --Spring.Echo(dump(units[i-startIndex+1]))
		SpringGiveOrderToUnit(units[i-startIndex+1],cmdID,{listOfUnits[i]},{})
	end

  return RUNNING

end



function Reset(self)
	ClearState(self)
end
