function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Stop and unload unit",
		parameterDefs = {
			{
				name = "transporterId", -- id of transporting unit
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<UnitIdOfTransporter>",
			},
			{
				name = "unitToUnloadId", -- id of transporter
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<IdOfTransportationUnit>",
			}
		}
	}
end
local function ClearState(self)
  self.unloading=false
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local transportId = parameter.transporterId -- unit with cargo to unload
	local unloadId = parameter.unitToUnloadId -- unit which should be unloaded


  if unloadId == nil or
    transportId == nil or
    Spring.GetUnitIsDead(unloadId) or
    Spring.GetUnitIsDead(transportId) then
    return SUCCESS
  end

  if  (Spring.GetUnitIsTransporting(transportId) == nil or
	#(Spring.GetUnitIsTransporting(transportId))==0 ) then
    self.unloading = false
    return SUCCESS
  end
  if self.unloading == nil or self.unloading == false then
    local x,y,z= Spring.GetUnitPosition(transportId)
    SpringGiveOrderToUnit(transportId,CMD.UNLOAD_UNIT,Vec3(x,Spring.GetGroundHeight(x,z),z):AsSpringVector(),{})
    self.unloading = true
  end
	--Spring.Echo("pracujem")
  return RUNNING
  --Spring.Echo(dump(parameter.listOfUnits))
  --[==[if cargo == isEmpty then
    return SUCCESS
  end]==]
end



function Reset(self)
	ClearState(self)
end
