function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Disperse units to Specific possitions",
		parameterDefs = {
			{
				name = "unitsToDisperse",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<ArayOfUnitIds>",
			}
		}
	}
end
local function ClearState(self)
  self.moveInitialized=false
end
local VELO_THRESHOLD = 2
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
		local locUnits = parameter.unitsToDisperse
	if self.moveInitialized==nil then
		self.moveInitialized = false
	end
	if self.moveInitialized then
		--check if any units are still moving
		local moving = false
		for i=1,#locUnits do
			local velx, vely, velz, velLength = Spring.GetUnitVelocity(locUnits[i])
			if math.abs(velx)+math.abs(vely)+math.abs(velz) >VELO_THRESHOLD then
				return RUNNING
			end
		end
		return SUCCESS
	end
	if self.moveInitialized == false then
		local positions = Sensors.nota_bal_heartlessai.PointsOnEdges(#locUnits)
		for i=1,math.min(#locUnits, #positions) do
			if not Spring.GetUnitIsDead(locUnits[i]) then
				SpringGiveOrderToUnit(locUnits[i],CMD.MOVE,(positions[i]):AsSpringVector(),{})
			end
			self.moveInitialized = true
		end
		return RUNNING
	else
		error("Unxpected state. Move initialized is "..self.moveInitialized.." and units are "..dump(locUnits))
	end


end



function Reset(self)
	ClearState(self)
end
