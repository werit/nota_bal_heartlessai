function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load unit",
		parameterDefs = {
			{
				name = "unitId", --unitIdOfUnitToLoad
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<IdOfUnitToLaod>",
			},
      {
				name = "loadingUnitId", --unitIdOfUnitPerformingLoading
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<IdOfTransporterUnit>",
			}
		}
	}
end
local function ClearState(self)
  self.loadingUnit = false
end

local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function Run(self, units, parameter)
	local unitToLoad = parameter.unitId
  local loaderId = parameter.loadingUnitId
	--Spring.Echo(dump(parameter.listOfUnits))

  if unitToLoad == nil or
  loaderId == nil or
  not Spring.ValidUnitID(unitToLoad) or
  not Spring.ValidUnitID(loaderId) or
	Spring.GetUnitDefID(loaderId) == nil or
	UnitDefs[Spring.GetUnitDefID(loaderId)].transportSize ==nil or
	UnitDefs[Spring.GetUnitDefID(loaderId)].transportSize <1 or
  (Spring.GetUnitIsTransporting(loaderId) ~= nil and
  #(Spring.GetUnitIsTransporting(loaderId))>0 ) then
    return SUCCESS
  end

  local myLoader = Spring.GetUnitTransporter(unitToLoad)
  if  myLoader~= nil and myLoader==loaderId then
    self.loadingUnit = false
    return SUCCESS
  end

  -- get unit def
  --local loaderDefId = Spring.GetUnitDefID(loaderId)
   --UnitDefs[loaderDefId].transportMass
  if self.loadingUnit == nil or self.loadingUnit == false then
    self.loadingUnit = true
    SpringGiveOrderToUnit(loaderId,CMD.LOAD_UNITS,{unitToLoad},{})
    return RUNNING
  end

return RUNNING

end



function Reset(self)
	ClearState(self)
end
