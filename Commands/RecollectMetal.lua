function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Collect metal on position in radius area from ground",
		parameterDefs = {
      {
				name = "unitId",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<idOfArmFark>",
			},
			{
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<Vec3(50,50,50)>",
			},
			{
				name = "radius", -- id of unit to move
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "<500>",
			}
		}
	}
end


local THRESHOLD_DEFAULT = 80
local THRESHOLD_DELTA = 15
local THRESHOLD_MIDDLE_STEP = 50
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitPosition = Spring.GetUnitPosition

local function ClearState(self)
  self.mining = false
--  self.lastPosition = Vec3(0,0,0)
end


local function initOnce(self)
--  if self.lastPosition == nil then
--    self.lastPosition = Vec3(0,0,0)
--  end
  if self.mining == nil then
    self.mining = false
  end
end

function Run(self, units, parameter)
  local miner = parameter.unitId -- unit to move along the path
  local minePos = parameter.position -- unit to move along the path
	local radius = parameter.radius -- unit to move along the path
	if miner == nil or not Spring.ValidUnitID(miner) then
    --[[if miner ~= nil then
      Spring.Echo(dump(Spring.ValidUnitID(miner))..dump(miner).."IsDead")
    end]]

		return SUCCESS
	end

  initOnce(self)



  if not self.mining then
    SpringGiveOrderToUnit(miner,CMD.RECLAIM,{minePos.x,minePos.y,minePos.z,radius},{})
    self.mining = true
    return RUNNING
  end

  if Spring.GetCommandQueue(miner,0) > 0  then
    return RUNNING
  end
  self.mining = false
  return SUCCESS
end



function Reset(self)
	ClearState(self)
end
